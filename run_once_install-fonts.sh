#!/bin/sh
fonts="${XDG_DATA_HOME:-$HOME/.local/share}"/fonts
mkdir -p "$fonts"
curl -fsLs https://github.com/microsoft/cascadia-code/releases/download/v2110.31/CascadiaCode-2110.31.zip -o /tmp/fonts.zip
# Remove outdated version
rm "$fonts"/CascadiaCode.ttf
umask 377
unzip -p /tmp/fonts.zip ttf/CascadiaCode.ttf>"$fonts"/CascadiaCode.ttf
rm /tmp/fonts.zip

gsettings set org.gnome.desktop.interface monospace-font-name 'Cascadia Code 10' || echo 'GSettings was unable to run, are you not running gnome?'
